import 'package:dropdown_button2/src/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matrimony/database.dart';
import 'package:matrimony/model/city_model.dart';

class AddUser extends StatefulWidget {
  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  late TextEditingController _nameController;

  GlobalKey<FormState> formKey = new GlobalKey();
  CityModel model =
      CityModel(city_id1: -1, stateid1: -1, city_name1: "SelectCity");
  bool isGetCity = true;
  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
  }

  String getFormattedDate(dateToFormat) {
    if (dateToFormat != null) {
      return DateFormat('dd-MM-yyyy').format(dateToFormat).toString();
    } else {
      return DateFormat('dd-MM-yyyy').format(DateTime.now());
    }
  }

  showAlertDialog(BuildContext context) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Please Select City"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _pickDateDialog() {
    showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1950),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return null;
      }
      setState(() {
        selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add User Screen"),
        backgroundColor: Colors.redAccent,
      ),
      body: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              width: double.infinity,
              child: FutureBuilder<List<CityModel>>(
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data != null) {
                    if (isGetCity == true) {
                      model = snapshot.data![0];
                    }
                    return DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        items: snapshot.data!
                            .map((item) => DropdownMenuItem<CityModel>(
                                  value: item,
                                  child: Text(
                                    item.city_name.toString(),
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ))
                            .toList(),
                        value: model,
                        onChanged: (value) {
                          setState(() {
                            model = value!;
                            isGetCity = false;
                          });
                        },
                        buttonStyleData: ButtonStyleData(
                          height: 50,
                          width: 160,
                          padding: const EdgeInsets.only(left: 14, right: 14),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              color: Colors.black26,
                            ),
                            color: Colors.redAccent,
                          ),
                          elevation: 2,
                        ),
                        iconStyleData: const IconStyleData(
                          icon: Icon(
                            Icons.arrow_drop_down_outlined,
                          ),
                          iconSize: 14,
                          iconEnabledColor: Colors.yellow,
                          iconDisabledColor: Colors.grey,
                        ),
                        dropdownStyleData: DropdownStyleData(
                          maxHeight: 200,
                          width: double.infinity,
                          padding: null,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            color: Colors.redAccent,
                          ),
                          elevation: 8,
                          offset: const Offset(-20, 0),
                          scrollbarTheme: ScrollbarThemeData(
                            radius: const Radius.circular(40),
                            thickness: MaterialStateProperty.all<double>(6),
                            thumbVisibility:
                                MaterialStateProperty.all<bool>(true),
                          ),
                        ),
                        menuItemStyleData: const MenuItemStyleData(
                          height: 40,
                          padding: EdgeInsets.only(left: 14, right: 14),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
                future:
                    isGetCity ? MyDatababase().getDataFromCityTable() : null,
              ),
            ),
            TextFormField(
              controller: _nameController,
              validator: (value) {
                if (value == null || value.trim().length == 0) {
                  return "TextEmpty";
                } else {
                  return null;
                }
              },
            ),
            InkWell(
              child: Text(getFormattedDate(selectedDate)),
              onTap: () {
                _pickDateDialog();
              },
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              color: Colors.redAccent,
              child: TextButton(
                onPressed: () {
                  if (!mounted) return;
                  setState(() {
                    if (formKey.currentState!.validate()) {
                      if (model.city_id == -1) {
                        showAlertDialog(context);
                      } else {
                        print(
                            "Data to be stored ... :: ${model.city_id.toString()}::${selectedDate.toString()}::${_nameController.text}");
                           MyDatababase().insertIntoUserTable(
                          cid: model.city_id,
                          dateofb: selectedDate.toString(),
                          uname: _nameController.text,
                        );
                      }
                    }
                  });
                },
                child: Text(
                  "Submit",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}