import 'package:flutter/material.dart';
import 'package:matrimony/add_user.dart';
import 'package:matrimony/database.dart';
import 'package:matrimony/model/city_model.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({super.key});

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  late Future<List<Map<String, Object?>>> userListFuture;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    MyDatababase().copyPasteAssetFileToRoot().then((value) => {
          print('Database Init Successfully!'),
          MyDatababase().getDataFromUserTable(),
        });
    // MyDatababase().getDataFromUserTable();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Icon(
              Icons.location_on,
              color: Colors.white,
            ),
            Text("City List"),
          ],
        ),
      ),
      // bottomNavigationBar: ,
      body: FutureBuilder<List<CityModel>>(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  child: Row(
                    children: [
                      Text((snapshot.data![index].city_id).toString()),
                      Text(
                        (snapshot.data![index].city_name).toString(),
                      ),
                    ],
                  ),
                );
              },
              itemCount: snapshot.data!.length,
            );
          } else {
            return const Center(
              child: Text("No User Found"),
            );
          }
        },
        future: MyDatababase().getDataFromCityTable(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddUser(),));
        },
        elevation: 4,
        tooltip: "Add",
        child: Icon(Icons.add,color: Colors.white,),
      ),
    );
  }
}
