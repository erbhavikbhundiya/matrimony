import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(
                "Hello",
                style: TextStyle(
                  color: Colors.red,
                  backgroundColor: Colors.red[20],
                  fontFamily: 'Poppins',
                  fontSize: 36,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 5, 2 , 20),
              color: Colors.black12,
              child: TextField(
                style: TextStyle(color: Colors.blue),
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: ((MediaQuery.of(context).size.width)-40)*0.58,
                    child: Container(
                        color: Colors.green,
                        margin: EdgeInsets.only(top: 20),
                        child: TextButton(
                            onPressed: () {},
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                        )
                    ),
                  ),
                  SizedBox(
                    width: ((MediaQuery.of(context).size.width)-40)*0.04,
                  ),
                  SizedBox(
                    width: ((MediaQuery.of(context).size.width)-40)*0.38,
                    child: Container(
                        color: Colors.red,
                        margin: EdgeInsets.only(top: 20),
                        child: TextButton(
                            onPressed: () {},
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                        )
                    ),
                  ),
                ],
              ),
            ),
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      ),
    );
  }
}
