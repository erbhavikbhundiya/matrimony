class UserModel{
  late int _userId;
  late int _city_id;
  late String _name;
  late String _dob;

  String get dob => _dob;

  set dob(String value){
    _dob = value;
  }

  int get userId => _userId;

  set userId(int value) {
    _userId = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  int get city_id => _city_id;

  set city_id(int value) {
    _city_id = value;
  }
}