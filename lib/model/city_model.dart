class CityModel{

  int stateid1;
  int city_id1;
  String city_name1;

  CityModel({required this.city_id1,required this.stateid1,required this.city_name1});
  int get city_id => city_id1;

  set city_id(int value) {
    city_id1 = value;
  }

  String get city_name => city_name1;

  set city_name(String value) {
    city_name1 = value;
  }

  int get stateid => stateid1;

  set stateid(int value) {
    stateid1 = value;
  }
}