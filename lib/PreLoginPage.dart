import 'package:flutter/material.dart';

class PreLoginPage extends StatefulWidget {
  @override
  State<PreLoginPage> createState() => _PreLoginPageState();
}

class _PreLoginPageState extends State<PreLoginPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(
                    "assets/images/bg.png",
                    fit: BoxFit.contain,
                    color: Colors.black,
                    colorBlendMode: BlendMode.color,
                  ),
                  Container(
                    color: Color.fromRGBO(0, 0, 0, 0.6),

                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Text(
                          "India\'s \nmost trusted\nMatrimonial Site!",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 32,
                            color: Colors.white,
                            height: 0.9,
                          ),
                        ),
                      ),
                      Container(
                        // width: (MediaQuery.of(context).size.width) * 0.8,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Container(
                                color: Colors.green,
                                child: TextButton(
                                    onPressed: () {},
                                    child:
                                    Text(
                                      "Login",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    )
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                color: Colors.black,
                                child: TextButton(
                                    onPressed: () {},
                                    child:
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Register",
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        ImageIcon(
                                          AssetImage("fonts/dblArrow.png"),
                                          color: Colors.white,
                                        ),
                                      ],
                                    )
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
