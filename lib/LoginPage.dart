import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  late TextEditingController _unmController;
  late TextEditingController _pwdController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _unmController = TextEditingController();
    _pwdController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Stack(
            alignment: Alignment.center,
            // fit:StackFit.expand,
            children: [
              Image.asset("assets/images/bg.png",
                fit: BoxFit.fill,
                repeat: ImageRepeat.noRepeat,
              ),
              Container(
                color: Color.fromRGBO(255, 255, 255, 40),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  // width: MediaQuery.of(context).size.width * 0.5,
                  decoration: BoxDecoration(
                    // border: ,
                      color: Colors.redAccent,
                      border: Border.all(
                        color: Colors.redAccent,
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(20.0),
                      ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("LOGIN",style: TextStyle(color: Colors.white,fontSize: 26),),
                      getField('UserName',Icons.person,_unmController,false),
                      getField('Password',Icons.remove_red_eye,_pwdController,true),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        decoration: BoxDecoration(
                          // border: ,
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                        ),
                        child: TextButton(
                            onPressed: () {
                              _checkval();
                            },
                            child: Text("Login",style: TextStyle(color: Colors.redAccent,),) ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
      )
    );
  }

  void _checkval(){
    setState(() {
      var u=_unmController.text;
      var p=_pwdController.text;
      print('$p::$u');
      _unmController.text = '';
      _pwdController.text = '';
    });
  }

  Widget getField(hint,IconData c,cont,bool val){
    return TextField(
      obscureText: val,
      obscuringCharacter: '#',
      controller: cont,
      style: TextStyle(
        color: Colors.white,
      ),

      decoration: InputDecoration(

        hintText: hint,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: 2.0,
            color: Colors.white,
          )
        ),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width:3.0,
              color: Colors.white,
            )
        ),

        hintStyle: TextStyle(
          color: Colors.white38,
        ),
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 15),
          child: Icon(c,color: Colors.white,),
        ),
      ),
    );

  }
}
