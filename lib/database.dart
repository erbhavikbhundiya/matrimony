import 'dart:io';

import 'package:flutter/services.dart';
import 'package:matrimony/model/user_model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'model/city_model.dart';

class MyDatababase{
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'database.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "database.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
      await rootBundle.load(join('assets/database', 'database.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
    }
  }
  
  Future<List<Map<String, Object?>>> getDataFromUserTable() async {
    Database db=await initDatabase();
    List<Map<String, Object?>> data =await db.rawQuery("select * from mst_state");
    print("USER_LIST::${data.length}");
    return data;
  }
  
  void insertIntoUserTable({cid,uname,dateofb}) async {
    Database db = await initDatabase();
    Map<String,Object> map =Map();
    map['name'] = uname;
    map['city_id']=cid;
    map['dob']=dateofb;
    print("INSERTCHECK ::${await db.insert("user_tbl ", map)}");
  }

  Future<List<CityModel>> getDataFromCityTable() async {
    List<CityModel> cityList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery("select * from mst_city");
    CityModel model = CityModel(
      city_id1: -1,
      stateid1: -1,
      city_name1: "SelectCity"
    );
    // model.city_id = -1;
    // model.city_name = "Select City";
    // model.stateid = -1;
    cityList.add(model);

    for (int i = 0; i < data.length; i++) {
      model = CityModel(
        city_id1: data[i]["city_id"] as int,
        city_name1: data[i]["city_name"] as String,
        stateid1: data[i]["stateid"] as int,
      );
      cityList.add(model);
    }
    return cityList;
  }
}