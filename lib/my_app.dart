import 'package:flutter/material.dart';
import 'package:matrimony/main.dart';
import 'package:matrimony/my_home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DemoTitle',
      theme: ThemeData(
          primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }

}