import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class InterestCalc extends StatefulWidget {
  @override
  State<InterestCalc> createState() => _InterestCalcState();
}

class _InterestCalcState extends State<InterestCalc> {
  final formatCurrency = new NumberFormat.simpleCurrency();
  var _calculatedInterest = '0';
  late TextEditingController _amountController;
  late TextEditingController _roiController;
  late TextEditingController _timeController;
  late TextEditingController _ans;


  void initState(){
    super.initState();
    _amountController = TextEditingController();
    _roiController = TextEditingController();
    _timeController = TextEditingController();
  }

  void _showResult(){
    setState(() {
      double amount = double.parse(_amountController.text);
      double rate = double.parse(_roiController.text);
      double time =  double.parse(_timeController.text);
      double calc = (amount * rate * time)/100;
      var ans = formatCurrency.format(calc);
      setAnswer(ans);
      // _amountController.text = "";
      // _roiController.text = "";
      // _timeController.text = "";
    });
}
  @override
  Widget build(BuildContext context) {
    var w =MediaQuery.of(context).size.width;
    var h=MediaQuery.of(context).size.height;


    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.black87,
            title : Container(

                child: Text('Simple Interest Calculator')),
        ),
        body:
        LayoutBuilder(builder: (context,constraints) => SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child:ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight,),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 20),
                            child: Column(
                              children: [
                                getAnswer(),
                                const Text(
                                  "Calculated interest",
                                  style: TextStyle(
                                    fontSize: 20,
                                    height: 0.01,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          getCard('Principal Amount',0.92,_amountController),
                        ],
                      ),
                      SizedBox(
                        height: h*0.02,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          getCard(
                              'Rate Of Interest',
                              0.45,
                              _roiController,
                          ),
                          SizedBox(
                            width: (w)*0.02,
                          ),
                          getCard(
                              "Number Of yearts",
                              0.45,
                              _timeController,
                          ),
                        ],
                      ),
                    ],
                  ),

                  SizedBox(
                    width: (MediaQuery.of(context).size.width)*0.92,
                      child: Container(
                        color:  Colors.amberAccent,
                        child: TextButton(
                            onPressed: () {
                              _showResult();
                            },
                            child: Text("Calculate",style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w800,
                            ),)
                        ),
                      )
                  ),
                ],
              ),
            ),
          ),
        ),)

      ),
    );
  }

  Widget getCard(String lbl,double scl,controller1){
    return SizedBox(
      width: ((MediaQuery.of(context).size.width) * scl),
      child: Container(
          decoration: BoxDecoration(
              color: Colors.black87,
              borderRadius: BorderRadius.circular(20),
          ),
          child: Container(
            // color: Colors.black87,
            padding: EdgeInsets.all(30),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(lbl,
                  style: TextStyle(
                    color: Colors.white54,
                  ),
                ),
                TextField(
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                  ),
                  controller: controller1,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '0',
                    hintStyle: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          )),
    );
  }

  void setAnswer(String ans){
    _calculatedInterest = ans;
  }
  Widget getAnswer(){
    return Text(
      '$_calculatedInterest',
      style: TextStyle(
        fontSize: 130,
        fontWeight: FontWeight.w500,
        height: 1.1,
      ),
    );
  }
}
